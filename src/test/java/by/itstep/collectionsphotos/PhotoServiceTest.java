package by.itstep.collectionsphotos;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.*;
import by.itstep.collectionsphotos.service.PhotoService;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.swing.text.html.parser.Entity;
import java.util.ArrayList;
import java.util.Arrays;

@SpringBootTest
public class PhotoServiceTest {

    private DatabaseCleaner dbCleaner;
    private PhotoService photoService;

    private UserRepository userRepository;
    private CollectionRepository collectionRepository;
    private PhotoRepository photoRepository;

    @BeforeEach
    public void setUp(){
        photoService=new PhotoService();

        userRepository=new UserHibernateRepository();
        collectionRepository=new CollectionHibernateRepository();
        photoRepository=new PhotoHibernateRepository();
        dbCleaner= new DatabaseCleaner();

        dbCleaner.clean();
    }

//    @Test
//    public void deleteById_happyPath(){
//        //given
//        UserEntity user= EntityUtils.prepareUser();
//        userRepository.create(user);
//
//        CollectionEntity collection = EntityUtils.prepareCollection();
//        collectionRepository.create(collection);
//
//        PhotoEntity photo=EntityUtils.preparePhoto();
//        photoRepository.create(photo);
//
//        collection.setPhotos(Arrays.asList(photo));
//        collectionRepository.upDate(collection);
//
//        //when
//
//        photoService.deleteById(photo.getId());
//        //then
//        Assertions.assertNotNull(userRepository.findById(user.getId()));
//        Assertions.assertNotNull (collectionRepository.findById(collection.getId()));
//
//        Assertions.assertNull(photoRepository.findById(photo.getId()));
//
//
//
   // }
}
