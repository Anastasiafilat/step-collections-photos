package by.itstep.collectionsphotos.entity;

import by.itstep.collectionsphotos.repository.UserRepository;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data  // <- это getter+setter+equals+hashcode+toString
@Entity
@Table(name="comments")
public class CommentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="message")
    private String message;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "photo_id")
    private PhotoEntity photo;
}
