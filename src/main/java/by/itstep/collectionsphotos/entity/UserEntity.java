package by.itstep.collectionsphotos.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.minidev.json.annotate.JsonIgnore;


import javax.persistence.*;
import java.util.List;

@Data  // <- это getter+setter+equals+hashcode+toString
@Entity
@Table(name="users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="login")
    private String login;

    @Column(name="password")
    private String password;

    @Column(name="email")
    private String email;

    @Column(name="name")
    private String name;

    @JsonIgnore
    @ToString.Exclude //чтобы не было цикличных зависимостей
    @EqualsAndHashCode.Exclude //чтобы не было цикличных зависимостей
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<CollectionEntity> collections;

    @JsonIgnore
    @ToString.Exclude //чтобы не было цикличных зависимостей
    @EqualsAndHashCode.Exclude //чтобы не было цикличных зависимостей
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<CommentEntity> comments;

}
