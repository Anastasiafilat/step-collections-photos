package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CollectionHibernateRepository implements CollectionRepository {

    @Override
    public CollectionEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        UserEntity foundCollection = em.find(UserEntity.class, id);

        em.getTransaction().commit();
        em.close();

        return null;
    }

    @Override
    public List<CollectionEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        List<CollectionEntity> allCollection =
                em.createNativeQuery("SELECT * FROM collections", UserEntity.class).getResultList();

        em.getTransaction().commit();
        em.close();

        return allCollection;
    }

    @Override
    public CollectionEntity create(CollectionEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(entity); //сохранить

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public CollectionEntity upDate(CollectionEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.merge(entity); //сохранить

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        CollectionEntity deleteCollection = em.find(CollectionEntity.class, id);
        em.remove(deleteCollection);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll(){
        EntityManager em=EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collections").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
