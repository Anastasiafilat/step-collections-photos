package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;

import java.util.List;

public interface CommentRepository {
    CommentEntity findById(int id);  //<- public

    List<CommentEntity> findAll();

    CommentEntity create(CommentEntity entity);

    CommentEntity upDate(CommentEntity entity);

    void deleteById(int id);

    void deleteAll();
}
