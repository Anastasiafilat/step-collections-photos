package by.itstep.collectionsphotos.repository;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;

import java.util.List;

public interface PhotoRepository {
    PhotoEntity findById(int id);  //<- public

    List<PhotoEntity> findAll();

    PhotoEntity create(PhotoEntity entity);

    PhotoEntity upDate(PhotoEntity entity);

    void deleteById(int id);

    void deleteAll();
}
