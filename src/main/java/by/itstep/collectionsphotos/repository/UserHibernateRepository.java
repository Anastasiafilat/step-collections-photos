package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.apache.catalina.User;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserHibernateRepository implements UserRepository{

    @Override
    public UserEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        UserEntity foundUser = em.find(UserEntity.class, id);

        if(foundUser!=null) {
            Hibernate.initialize(foundUser.getCollections());
            Hibernate.initialize(foundUser.getComments());
        }

        em.getTransaction().commit();
        em.close();

        return foundUser;
    }

    @Override
    public List<UserEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        List<UserEntity> allUsers =
                em.createNativeQuery("SELECT * FROM users", UserEntity.class).getResultList();

        for (UserEntity user: allUsers){
            Hibernate.initialize(user.getCollections());
            Hibernate.initialize(user.getComments());
        }

        em.getTransaction().commit();
        em.close();

        return allUsers;
    }

    @Override
    public UserEntity create(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        em.persist(entity); //сохранить

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public UserEntity upDate(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        UserEntity entityUpDate = em.find(UserEntity.class, entity.getId());
        entityUpDate.setName(entity.getName());
        entityUpDate.setEmail(entity.getEmail());
        entityUpDate.setLogin(entity.getLogin());
        entityUpDate.setPassword(entity.getPassword());

        em.getTransaction().commit();
        em.close();

        return null;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        UserEntity deleteUser = em.find(UserEntity.class, id);
        em.remove(deleteUser);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll(){
        EntityManager em=EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM users").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
