package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;

import java.util.List;

public interface CollectionRepository {

    CollectionEntity findById(int id);  //<- public

    List<CollectionEntity> findAll();

    CollectionEntity create(CollectionEntity entity);

    CollectionEntity upDate(CollectionEntity entity);

    void deleteById(int id);

    void deleteAll();
}
