package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.config.SecurityService;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.*;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.config.ScheduledTaskHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private SecurityService securityService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private CommentRepository commentRepository;

//    public UserService(){
//      userRepository = ServiceFactory.getUserRepository();
//      collectionRepository=ServiceFactory.getCollectionRepository();
//      commentRepository=ServiceFactory.getCommentRepository();
//    }

    public UserEntity findById(int id){
        UserEntity user =userRepository.findById(id);
        if(user==null){
            throw new RuntimeException("User not found by id: "+id);
        }
        System.out.println("UserService found user: "+user);
        return user;
    }

    public List<UserEntity> findAll(){
        Authentication auth= SecurityContextHolder.getContext().getAuthentication();

        System.out.println("------->" + auth.getName());

        List<UserEntity>allUsers=userRepository.findAll();
        System.out.println("UserService found user: "+allUsers);
        return allUsers;
    }

    public  UserEntity create(UserEntity user){
        if (user.getId()!=null){
            throw new RuntimeException("UserService-> Can't create entity which already has id");
        }
        List<UserEntity>existingUsers=userRepository.findAll();
        for (UserEntity existingUser:existingUsers){
            if(existingUser.getEmail().equals(user.getEmail())){
                throw new RuntimeException("?");
            }
        }
        UserEntity createdUser=userRepository.create(user);
        return createdUser;
    }

    public UserFullDto update(UserUpdateDto updateRequest){
        UserEntity currentUser=securityService.getAuthenticatedUser();
        if(currentUser.getId().equals(updateRequest.getId())){
            throw new RuntimeException("UserService-> Can't update another user!");
        }

        if(updateRequest.getId()==null){
            throw new RuntimeException("UserService -> Can't update entity without id");
        }

        if (userRepository.findById(user.getId())==null) {
            throw new RuntimeException("UserService -> User not found by id: "+ user.getId());
        }
        UserEntity updatedUser=userRepository.upDate(user);
        System.out.println("UserService -> Updated user: "+user);

        return updatedUser;
    }

    public void deleteById(int id){
        UserEntity userToDelete=userRepository.findById(id);
        if(userToDelete==null){
            throw new RuntimeException("User was not found by id: "+id);
        }
        List<CollectionEntity> existingCollection=userToDelete.getCollections();
        List<CommentEntity> existingComments=userToDelete.getComments();

        for (CollectionEntity collection:existingCollection) {
            collectionRepository.deleteById(collection.getId());
        }
        for (CommentEntity comment:existingComments) {
            commentRepository.deleteById(comment.getId());

        }
        userRepository.deleteById(id);
    }

    }


