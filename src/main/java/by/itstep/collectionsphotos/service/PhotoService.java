package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhotoService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private PhotoRepository photoRepository;

//    public PhotoService(){
//
//        collectionRepository= ServiceFactory.getCollectionRepository();
//        photoRepository=ServiceFactory.getPhotoRepository();;
//    }

    public PhotoEntity findById(int id){
        PhotoEntity photo = photoRepository.findById(id);
        if(photo==null){
            throw new RuntimeException("Photo not found by id: "+id);
        }
        System.out.println("PhotoService found photo: "+photo);
        return photo;
    }

    public List<PhotoEntity> findAll(){

        List<PhotoEntity> allPhotos= photoRepository.findAll();
        System.out.println("PhotoService found photo: " + allPhotos);
        return allPhotos;
    }


    public PhotoEntity create(PhotoEntity photo){
        if (photo.getId() == null) {
            throw new RuntimeException("Can't created photo");
        }

       PhotoEntity createdPhoto = photoRepository.create(photo);
        System.out.println("Photo create! "+ createdPhoto);
        return createdPhoto;
    }

    public PhotoEntity update(PhotoEntity photo){
        if (photo.getId() == null) {
            throw new RuntimeException("Can't update photo");
        }

        if (photoRepository.findById(photo.getId()) == null) {
            throw new RuntimeException("PhotoService -> Photo not found by id: " + photo.getId());
        }

        PhotoEntity updatedPhoto = photoRepository.upDate(photo);
        System.out.println("CollectionService: Collection update: " + updatedPhoto);
        return updatedPhoto;
    }

    public void deleteById(int id) {
        PhotoEntity photoToDelete = photoRepository.findById(id);
       List<CollectionEntity> photoCollections=photoToDelete.getCollections();

       for(CollectionEntity collection:photoCollections){
           collection.getPhotos().remove(photoToDelete);
           collectionRepository.upDate(collection);
       }

       photoRepository.deleteById(id);
        System.out.println("Photo deleted!");
    }
}
