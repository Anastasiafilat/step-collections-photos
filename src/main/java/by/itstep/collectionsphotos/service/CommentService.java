package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private PhotoRepository photoRepository;

//    public CommentService (){
//        userRepository = new UserHibernateRepository();
//        collectionRepository=new CollectionHibernateRepository();
//        commentRepository=new CommentHibernateRepository();
//        photoRepository=new PhotoHibernateRepository();
//    }

    public CommentEntity findById(int id){
        CommentEntity comment = commentRepository.findById(id);
        if(comment==null){
            throw new RuntimeException("Comment not found by id: "+id);
        }
        System.out.println("CommentService found photo: "+comment);
        return comment;
    }

    public List<CommentEntity> findAll(){

        List<CommentEntity> allComments= commentRepository.findAll();
        System.out.println("CommentService found comment: " + allComments);
        return allComments;
    }


    public CommentEntity create(CommentEntity comment){
        if (comment.getId() == null) {
            throw new RuntimeException("Can't created comment");
        }

        CommentEntity createdComment = commentRepository.create(comment);
        System.out.println("Comment create! "+ createdComment);
        return createdComment;
    }

    public CommentEntity update(CommentEntity comment){
        if (comment.getId() == null) {
            throw new RuntimeException("Can't update comment");
        }

        if (commentRepository.findById(comment.getId()) == null) {
            throw new RuntimeException("CommentService -> Comment not found by id: " + comment.getId());
        }

        CommentEntity updatedComment = commentRepository.upDate(comment);
        System.out.println("CommentService: Comment update: " + updatedComment);
        return updatedComment;
    }

    public void deleteById(int id) {
        CommentEntity commentToDelete = commentRepository.findById(id);
        if (commentToDelete == null) {
            throw new RuntimeException("Comment was not found by id: " + id);
        }
        commentRepository.deleteById(id);
        System.out.println("Comment deleted!");
    }
}
