package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.*;
import org.hibernate.loader.plan.exec.internal.AbstractLoadPlanBasedLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class CollectionService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private CommentRepository commentRepository;

//    public CollectionService() {
//        userRepository = new UserHibernateRepository();
//        collectionRepository = new CollectionHibernateRepository();
//        commentRepository = new CommentHibernateRepository();
//    }

    // Поиск по id
    // * Не забыть что бросаем исключение если не нашли
    public CollectionEntity findById(int id) {
        CollectionEntity collection = collectionRepository.findById(id);
        if (collection == null) {
            throw new RuntimeException("Collection not found by id: " + id);
        }
        return collection;
    }

    // Поиск всех
    // * Исключение НЕ выбрасываем даже если список пустой
    public List<CollectionEntity> findAll() {
        List<CollectionEntity> allCollections = collectionRepository.findAll();
        System.out.println("CollectionService found collection: " + allCollections);
        return allCollections;
    }
    // Создание
    // * Нужно проверить что айдишник в коллекции НЕ указан
    // * Нужно проверить что такой пользователь есть

    public CollectionEntity create(CollectionEntity collection) {
        if (collection.getId() != null) {
            throw new RuntimeException("Can't created collection with existing id");
        }

        if (collection.getUser().getId()==null) {
            throw new RuntimeException("Can't found user with this id");
        }

        if(userRepository.findById(collection.getUser().getId())==null){
            throw new RuntimeException("This user no found in database");
        }

        CollectionEntity createdCollection = collectionRepository.create(collection);
        System.out.println("Collection create! "+ createdCollection);
        return createdCollection;
    }
    // Обновление
    // * Нужно проверить что айдишник в коллекции указан
    // * Нужно проверить что такая коллекция есть
    // * Нужно проверить что такой пользователь есть

    public CollectionEntity update(CollectionEntity collection) {
        if (collection.getId() == null) {
            throw new RuntimeException("Can't update collection");
        }

        if (collectionRepository.findById(collection.getId()) == null) {
            throw new RuntimeException("CollectionService -> Collection not found by id: " + collection.getId());
        }

        if(userRepository.findById(collection.getUser().getId())==null){
            throw new RuntimeException("CollectionService-> User not found by id: "+collection.getUser());
        }
        CollectionEntity updatedCollection = collectionRepository.upDate(collection);
        System.out.println("CollectionService: Collection update: " + collection);
        return updatedCollection;
    }

    // Удаление по id
    // * Нужно проверить что такая коллекция есть
    // * Нужно удалить ТОЛЬКО коллекцию (фотки не трогать!)
    public void deleteById(int id) {
        CollectionEntity collectionToDelete = collectionRepository.findById(id);
        if (collectionToDelete == null) {
            throw new RuntimeException("Collection was not found by id: " + id);
        }

        collectionRepository.deleteById(id);
        System.out.println("Collection deleted!");
    }

}

