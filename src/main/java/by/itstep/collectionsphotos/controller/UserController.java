package by.itstep.collectionsphotos.controller;

import by.itstep.collectionsphotos.controller.dto.UserDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    //@GetMapping("/users")
    @ResponseBody
    @RequestMapping(value="/users", method = RequestMethod.GET)
    public List<UserDto> findAllUsers(){
        List<UserEntity> allUsers=userService.findAll();
        //put allUsers to html file
        return allUsers;
    }

    @ResponseBody
    @RequestMapping(value="/users/{id}", method=RequestMethod.GET)
    public UserEntity findById(@PathVariable int id){
        UserEntity user=userService.findById(id);

        return user;
    }

    @ResponseBody
    @RequestMapping(value="/users", method=RequestMethod.POST)
    public UserEntity create(@RequestBody UserEntity createRequest){
        UserEntity createdUser=userService.create((createRequest));

        return createdUser;
    }

    @ResponseBody
    @RequestMapping(value="/users/{userId}/photos/{photoId}/comments", method=RequestMethod.POST)
    public CommentEntity create(@RequestBody CommentEntity createRequest){
       CommentEntity createdComment=commentService.create((createRequest, userId, photoId));

        return createdComment;
    }

    class CommentService{

        public CommentEntity create (CommentEntity comment, Integer userId, Integer photoId){
          UserEntity user = userRepository.findById(userId);
          PhotoEntity photo = photoRepository.findById(photoId);

          comment.setUser(user);
          comment.setPhoto(photo);

          CommentEntity created = repository.create(comment);
           return created;
        }
    }



    @ResponseBody
    @RequestMapping(value="/users", method=RequestMethod.PUT)
    public UserEntity update(@RequestBody UserEntity updateRequest){
        UserEntity updatedUser=userService.update(updateRequest);

        return updatedUser;
    }

    @ResponseBody
    @RequestMapping(value="/users/{id}", method=RequestMethod.DELETE)
    public void delete(@PathVariable int id){
        userService.deleteById(id);
    }

}
