package by.itstep.collectionsphotos.controller.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDto {

    private Integer id;
    private String name;
    private String login;
    private String email;
    private String password;
    private List<> collections;
    private List<>comments;

}
